package com.example.testgitflow;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestGitflowApplication {

  public static void main(String[] args) {
    SpringApplication.run(TestGitflowApplication.class, args);
  }

}
