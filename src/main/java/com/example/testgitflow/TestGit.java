package com.example.testgitflow;

import java.util.List;

/**
 * this is test git
 */
public class TestGit {
  private final String name;
  private final List<TestVersion> versions;

  public TestGit(String name, List<TestVersion> versions) {
    this.name = name;
    this.versions = versions;
  }

  public String getName() {
    return name;
  }

  public List<TestVersion> getVersions() {
    return versions;
  }
}
