package com.example.testgitflow;

public class TestVersion {
  private final String version;

  public TestVersion(String version) {
    this.version = version;
  }

  public String getVersion() {
    return version;
  }
}
